package com.amex.erem.csv.parser;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.nio.file.NoSuchFileException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvToJsonParser {

    private final String filePath;
    private final boolean withHeaders;

    public CsvToJsonParser(String filePath, boolean withHeaders)throws NoSuchFileException {
        this.filePath = filePath;
        this.withHeaders = withHeaders;
        if(!(new File(filePath).exists()))
            throw new NoSuchFileException(filePath);
    }

    public CsvToJsonParser(String filePath)throws NoSuchFileException {
        this(filePath, true);
    }

    public void parseAsJsonIntoFile(String destinationFilePath) {

    }

    public String parseAsJson() throws Exception {
        String json = null;
        if(withHeaders) {
            json = parseAsJsonWithHeaders();
        }else{
            json = null;
        }
        return json;
    }

    private String parseAsJsonWithHeaders() throws Exception {
        Reader in = new FileReader(filePath);
        final CSVFormat formatWithFirstRecordAsHeader = CSVFormat.DEFAULT.withFirstRecordAsHeader();
        Iterable<CSVRecord> records = formatWithFirstRecordAsHeader.parse(in);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        List<Map<String, String>> content = new ArrayList<>();
        for (CSVRecord record : records) {
            Map<String, String> map = record.toMap();
            if("TOTAL".equalsIgnoreCase(map.get("BTA_ACCT"))){
                String total = map.get("ALLOCATED_AMT");
                map = new HashMap<>();
                map.put("TOTAL", total);
            }
            content.add(map);
        }
        return gson.toJson(content);
    }



    public static void main(String[] args) throws Exception {
        String filePath = "/Users/balu/TestFiles/GR.JAPA.BTA.csv";
        CsvToJsonParser parser = new CsvToJsonParser(filePath);
        String json = parser.parseAsJson();
        System.out.println(json);
    }
}
