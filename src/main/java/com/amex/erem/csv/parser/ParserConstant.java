package com.amex.erem.csv.parser;
import javax.swing.text.html.parser.Parser;

public abstract class ParserConstant {
    private ParserConstant(){}
    public static final String LINE_NO = "LINE_NO";
    public static final String HEADER = "HEADER";
    public static final String ALLOCATED_AMT = "ALLOCATED_AMT";
    public static final String BTA_ACCT = "BTA_ACCT";
    public static final String COMMENTS = "COMMENTS";
    public static final String INV_NO = "INV_NO";
    public static final String STMT_REF_NO = "STMT_REF_NO";
    public static final String[] HEADERS = {BTA_ACCT, STMT_REF_NO, ALLOCATED_AMT, INV_NO, COMMENTS};

    public static final String TOTAL = "TOTAL";

}
