package com.amex.erem.csv.parser.vo;

import com.amex.erem.csv.parser.ParserConstant;
import com.google.gson.annotations.SerializedName;

public class Allocation implements Statement{

    @SerializedName(ParserConstant.LINE_NO)
    private String lineNo;

    @SerializedName(ParserConstant.ALLOCATED_AMT)
    private String allocatedAmount;

    @SerializedName(ParserConstant.BTA_ACCT)
    private String btaAccount;

    @SerializedName(ParserConstant.COMMENTS)
    private String comments;

    @SerializedName(ParserConstant.INV_NO)
    private String invoceNumber;

    @SerializedName(ParserConstant.STMT_REF_NO)
    private String statementReferenceNumber;

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public String getAllocatedAmount() {
        return allocatedAmount;
    }

    public void setAllocatedAmount(String allocatedAmount) {
        this.allocatedAmount = allocatedAmount;
    }

    public String getBtaAccount() {
        return btaAccount;
    }

    public void setBtaAccount(String btaAccount) {
        this.btaAccount = btaAccount;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getInvoceNumber() {
        return invoceNumber;
    }

    public void setInvoceNumber(String invoceNumber) {
        this.invoceNumber = invoceNumber;
    }

    public String getStatementReferenceNumber() {
        return statementReferenceNumber;
    }

    public void setStatementReferenceNumber(String statementReferenceNumber) {
        this.statementReferenceNumber = statementReferenceNumber;
    }

    public Allocation(String lineNo, String allocatedAmount, String btaAccount, String comments, String invoceNumber, String statementReferenceNumber) {
        this.lineNo = lineNo;
        this.allocatedAmount = allocatedAmount;
        this.btaAccount = btaAccount;
        this.comments = comments;
        this.invoceNumber = invoceNumber;
        this.statementReferenceNumber = statementReferenceNumber;
    }

    public Allocation() {
        super();
    }

    @Override
    public String toString() {
        return "Allocation{" +
                "lineNo='" + lineNo + '\'' +
                ", allocatedAmount='" + allocatedAmount + '\'' +
                ", btaAccount='" + btaAccount + '\'' +
                ", comments='" + comments + '\'' +
                ", invoceNumber='" + invoceNumber + '\'' +
                ", statementReferenceNumber='" + statementReferenceNumber + '\'' +
                '}';
    }
}
