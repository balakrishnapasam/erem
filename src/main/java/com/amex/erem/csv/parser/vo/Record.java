package com.amex.erem.csv.parser.vo;

import java.util.ArrayList;
import java.util.List;

public class Record {
    private Header headers;
    private List<Allocation> allocations;
    private AllocationTotal total;

    public Header getHeaders() {
        return headers;
    }

    public void setHeaders(Header headers) {
        this.headers = headers;
    }

    public List<Allocation> getAllocations() {
        if(allocations==null) allocations = new ArrayList<>();
        return allocations;
    }

    public void setAllocations(List<Allocation> allocations) {
        this.allocations = allocations;
    }

    public AllocationTotal getTotal() {
        return total;
    }

    public void setTotal(AllocationTotal total) {
        this.total = total;
    }
}
