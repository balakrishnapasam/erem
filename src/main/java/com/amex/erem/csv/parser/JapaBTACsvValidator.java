package com.amex.erem.csv.parser;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JapaBTACsvValidator {

    private static final String ALLOCATED_AMT = "ALLOCATED_AMT";
    private static final String BTA_ACCT = "BTA_ACCT";
    /**
     * Validates the json and throws exception if validation fails.
     * @param json a string containing json
     */
    public void validate(String json){
        Gson gson = new Gson();
        List<Map<String, String>> records = gson.fromJson(json, ArrayList.class);
        Long total = 0l;
        for(Map<String, String> record : records) {
            boolean canContinue = checkTotal(total, record);
            if(canContinue) continue;
            total += getAllocatedAmount(record);
            validateAccountNumber(record);

        }
    }

    private void validateAccountNumber(Map<String, String> record) {
        if(record.containsKey(BTA_ACCT)){
            String btaAccountString = record.get(BTA_ACCT);
            Long btaAccountNum = 0l;
            try{
                btaAccountNum = Long.parseLong(btaAccountString);
            }catch(Exception e){
                throw new RuntimeException("Invalid "+BTA_ACCT+" : "+btaAccountString, e);
            }
            String longValue = String.format("%.0f", btaAccountNum);
            if(longValue.length()<15)
                throw new RuntimeException("Account length is less than 15, "+BTA_ACCT+" : "+btaAccountString);
        }else{
            throw new RuntimeException(BTA_ACCT+" field is not available");
        }
    }

    /**
     * get the allocated amount and throws exception if the amount string is not a valid number.
     * @param record
     * @return
     */
    private Long getAllocatedAmount(Map<String, String> record) {
        if(record.containsKey(ALLOCATED_AMT)){
            String allocatedAmountString = record.get(ALLOCATED_AMT);
            try{
                return Long.parseLong(allocatedAmountString);
            }catch(Exception e){
                throw new RuntimeException("Invalid "+ALLOCATED_AMT+" : "+allocatedAmountString, e);
            }
        }else{
            throw new RuntimeException("ALLOCATED_AMT field is not available");
        }
    }

    /**
     * Checks if the total value is sum of allocated amount else throws exception.
     * @param total
     * @param record
     * @return
     */
    private boolean checkTotal(Long total, Map<String, String> record) {
        try {
            if (record.containsKey("TOTAL")) {
                Long totalValue = Long.parseLong(record.get("TOTAL"));
                if (totalValue != total)
                    throw new RuntimeException();
                return true;
            }
            return false;
        }catch(Exception e){
            throw new RuntimeException("TOTAL field is not available");
        }
    }

}
