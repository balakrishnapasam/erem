package com.amex.erem.csv.parser.v3;

import com.amex.erem.csv.parser.ParserConstant;
import com.amex.erem.csv.parser.vo.Allocation;
import com.amex.erem.csv.parser.vo.AllocationTotal;
import com.amex.erem.csv.parser.vo.Header;
import com.amex.erem.csv.parser.vo.Record;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class CsvToJsonParser {

    private static final Logger logger = LoggerFactory.getLogger(CsvToJsonParser.class);

    public CsvToJsonParser() {
        super();
    }

    public List<Record> parse(String csvContent) throws Exception {
        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        logger.info("Request came to parse csv to json.");
        List<Record> result = new ArrayList<>();
        Record record = new Record();
        String[] headers = null;
        int i = 0;
        for(String ln : csvContent.split("\n")) {
            i++;
            logger.debug("Processing line : "+i);
            String line = ln.trim();
            if(line.matches("[a-zA-Z,_]+")){
                logger.debug("The line contains header : "+line);
                if(!record.getAllocations().isEmpty()){
                    logger.debug("Creating new record as old record was not empty");
                    result.add(record);
                    record = new Record();
                }
                headers = line.split(",");
                Map<String, String> headerMap = new HashMap<>();
                headerMap.put(ParserConstant.LINE_NO, new Integer(i).toString());
                for(int a=0;a<headers.length;a++){
                    headerMap.put("HEADER"+(a+1), headers[a]);
                }
                Header header = gson.fromJson(gson.toJson(headerMap), Header.class);
                record.setHeaders(header);
                continue;
            }

            if(headers==null){
                logger.debug("Default header will be used as the header are not contained in.");
                headers = ParserConstant.HEADERS;
                Map<String, String> headerMap = new HashMap<>();
                headerMap.put(ParserConstant.LINE_NO, new Integer(i).toString());
                for(int a=0;a<headers.length;a++){
                    headerMap.put("HEADER"+(a+1), headers[a]);
                }
                Header header = gson.fromJson(gson.toJson(headerMap), Header.class);
                record.setHeaders(header);
            }

            if(line.contains(ParserConstant.TOTAL)){
                logger.debug("Line contains total : "+line);
                String total  = getTotal(line);
                Map<String, String> map = new HashMap<>();
                map.put(ParserConstant.LINE_NO, new Integer(i).toString());
                map.put(ParserConstant.TOTAL, total);
                record.setTotal(gson.fromJson(gson.toJson(map), AllocationTotal.class));
                result.add(record);
                Header lastHeaders = record.getHeaders();
                record = new Record();
                record.setHeaders(lastHeaders);
                continue;
            }
            logger.debug("Line contains data : "+line);
            Map<String, String> map = new HashMap<>();
            String[] cells = line.split(",");
            map.put(ParserConstant.LINE_NO, new Integer(i).toString());
            for(int a=0;a<headers.length && a<cells.length;a++) {
                map.put(headers[a], cells[a]==null?"":cells[a]);
            }
            for(int a=0;a<headers.length;a++) {
                if(!map.containsKey(headers[a]))
                    map.put(headers[a], "");
            }
            record.getAllocations().add(gson.fromJson(gson.toJson(map), Allocation.class));
        }
        logger.info("CSV is converted to objects. Further it can transformed into json");
        return result;
    }

    private String getTotal(String line) {
        for(String con : line.split(",")) {
            if(!con.isEmpty() && !ParserConstant.TOTAL.equalsIgnoreCase(con))
                return con;
        }
        return "";
    }

}
