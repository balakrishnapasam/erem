package com.amex.erem.csv.parser.vo;

import com.amex.erem.csv.parser.ParserConstant;
import com.google.gson.annotations.SerializedName;

public class Header {

    @SerializedName(ParserConstant.LINE_NO)
    String lineNo;
    @SerializedName(ParserConstant.HEADER+1)
    private String header1;
    @SerializedName(ParserConstant.HEADER+2)
    private String header2;
    @SerializedName(ParserConstant.HEADER+3)
    private String header3;
    @SerializedName(ParserConstant.HEADER+4)
    private String header4;
    @SerializedName(ParserConstant.HEADER+5)
    private String header5;

    public String getLineNo() {
        return lineNo;
    }

    public void setLineNo(String lineNo) {
        this.lineNo = lineNo;
    }

    public String getHeader1() {
        return header1;
    }

    public void setHeader1(String header1) {
        this.header1 = header1;
    }

    public String getHeader2() {
        return header2;
    }

    public void setHeader2(String header2) {
        this.header2 = header2;
    }

    public String getHeader3() {
        return header3;
    }

    public void setHeader3(String header3) {
        this.header3 = header3;
    }

    public String getHeader4() {
        return header4;
    }

    public void setHeader4(String header4) {
        this.header4 = header4;
    }

    public String getHeader5() {
        return header5;
    }

    public void setHeader5(String header5) {
        this.header5 = header5;
    }

    public Header() {
    }
}
