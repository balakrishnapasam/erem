package com.amex.erem.csv.parser.vo;

import com.amex.erem.csv.parser.ParserConstant;
import com.google.gson.annotations.SerializedName;

public class AllocationTotal implements Statement {

    @SerializedName(ParserConstant.LINE_NO)
    private String lineNo;

    @SerializedName("TOTAL")
    private Double total;

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public AllocationTotal(String lineNo, Double total) {
        this.lineNo = lineNo;
        this.total = total;
    }

    @Override
    public String toString() {
        return "AllocationTotal{" +
                "lineNo='" + lineNo + '\'' +
                ", total=" + total +
                '}';
    }
}
