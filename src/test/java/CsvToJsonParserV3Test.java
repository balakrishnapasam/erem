import com.amex.erem.csv.parser.v3.CsvToJsonParser;
import com.amex.erem.csv.parser.vo.Record;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.List;

public class CsvToJsonParserV3Test {

    @Test
    public void testParserNormal() throws Exception {
        String fileName = "/GR.JAPA.BTA.random.place.header.csv";
        String fileContent = getFileContent(fileName);
        CsvToJsonParser parser = new CsvToJsonParser();
        List<Record> statements = parser.parse(fileContent);
        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String json = gson.toJson(statements);
        System.out.println(json);
    }

    private String getFileContent(String fileName) throws Exception {
       InputStream in = ClassLoader.class.getResourceAsStream(fileName);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        IOUtils.copy(in, bos);
        String fileContent = new String (bos.toByteArray(), "utf-8");
        return fileContent;
    }
}
